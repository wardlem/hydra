defmodule Hydra do
  @moduledoc """
  Documentation for Hydra.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Hydra.hello()
      :world

  """
  def hello do
    :world
  end
end
